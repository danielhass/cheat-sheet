# cheat-sheet

[[_TOC_]]

### Arch Linux

#### Disable *-debug package generation

```
# /etc/makepkg.conf
OPTIONS=([...] !debug [...])
```

Source: https://man.archlinux.org/man/makepkg.conf.5#OPTIONS

### QNAP

#### TERM and TERMINFO settings for screen

```
export TERMINFO=/usr/share/terminfo
export TERM=xterm-xfree86
screen -S <name>
```

### Unifi / Ubiquity

#### desec.io DynDNS

Configure the DynDNS Service on the WAN interface like this:

- Service: `dyndns`
- Hostname: `<fqdn to be changed>`
- Username: `<fqdn to be changed>`
- Password: `<token>`
- Server: `update.dedyn.io/?myipv4=%i`

Source: https://talk.desec.io/t/udm-pro-configuration/317/2

### Docker

#### Remove Container auto/restart setting

```
docker update --restart=no <container-name>
```

### ZFS

#### Disk replacement

- `zpool status` - get status of pools
- `ls -l /dev/disk/by-id/*` - get list of disks by id
- `sgdisk /dev/<source> -R /dev/<target>` - copy disk layout
- `sgdisk -G /dev/<target>` - randomize GUIDs
- `zpool replace <pool-name> /dev/disk/by-id/<disk-id-existing>-part3 /dev/disk/by-id/<disk-id-new>-part3`
    - if the to be replaced device is unavailable we can also use the disk identifier/name from the `zpool status` command output
        - e.g. `zpool replace <pool-name> <zpool-status-name> /dev/disk/by-id/<disk-id-new>-part3`
- `zpool status` - watch resilvering status; old device will be removed when finished
- `proxmox-boot-tool format /dev/disk/by-id/<disk-id-new>-part2` - format boot partition
- `proxmox-boot-tool init /dev/disk/by-id/<disk-id-new>-part2` - init bootloader
- `proxmox-boot-tool status` - check bootloader status
- `proxmox-boot-tool clean` - cleanup entry of replaced disk

Sources:
- https://www.thomas-krenn.com/de/wiki/Boot-Device_Replacement_-_Proxmox_ZFS_Mirror_Disk_austauschen
- https://rephlex.de/blog/2022/10/31/proxmox-replace-zfs-root-disk/

### Spotify

#### Debug desktop app

- Start Spotify with debug port: `Spotify.exe --remote-debugging-port=9222`
- Use Chrome to attach: URL `chrome://inspect/`

Ref: https://github.com/spicetify/spicetify-cli/issues/1518#issuecomment-1060122063

### 7-zip

#### Command line reference wizard

- https://axelstudios.github.io/7z/#!/

#### Encrypt without compression

```
7z a -m0=copy -p<password-leave-empty-for-prompt> ./<archive-name>.7z ./<file-or-directory-to-add>
```

### Misc

#### Get CA certificates from server

```
curl https://curl.se -w "%{certs}" -o /dev/null > cacert.pem
```

- Remark: this is a rather new functionallity of curl and is still a pending release: [Haxx Blog](https://daniel.haxx.se/blog/2022/12/28/curl-w-certs/)

---

Repo icon created by [Freepik - Flaticon](https://www.flaticon.com/free-icons/data)
